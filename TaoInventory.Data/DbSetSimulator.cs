﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using TaoInventory.Models;

namespace TaoInventory.Data
{
    public class DbSetSimulator<TEntity> : DbSet<TEntity>
        where TEntity : BaseModel
    {
        public DbSetSimulator(Dictionary<Guid, TEntity> set)
        {
            this.set = set;
        }

        protected Dictionary<Guid, TEntity> set;

        public IQueryProvider Provider { get { return set.AsQueryable().Provider;  } }
        public Expression Expression { get { return set.AsQueryable().Expression; } }
        public Type ElementType { get { return set.AsQueryable().ElementType; } }
        public IEnumerator GetEnumerator { get { return set.AsQueryable().GetEnumerator(); } }
        public override TEntity Add(TEntity row)
        {
            set.Add(row.Id, row);
            return row;
        }
    }
}
