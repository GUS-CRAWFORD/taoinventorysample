﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TaoInventory.Models;

namespace TaoInventory.Data
{
    public interface IRepo<TContext, TDomain>
            where TDomain : BaseModel
            where TContext : IDbContext
    {

        IRepo<TContext, TDomainChild> ToRepo<TDomainChild>() where TDomainChild : TDomain;
        IRepo<TContext, TDomain> Include(params string[] includedProperties);

        IRepo<TContext, TDomain> AsNoTracking();

        IRepo<TContext, TDomain> Query(
                int page = 0,
                int perPage = 0,
                params string[] orderBy);

        IRepo<TContext, TDomain> Query(
                Expression<Func<TDomain, bool>> predicate,
                int page = 0,
                int perPage = 0,
                params string[] orderBy);

        IRepo<TContext, TDomain> Query<TResult>(
                Expression<Func<TDomain, TResult>> select,
                Expression<Func<TDomain, bool>> predicate,
                int page = 0,
                int perPage = 0,
                params string[] orderBy);

        TDomain Find(params object[] keyValues);

        void Upsert(TDomain item);

        QueryDetails Details { get; }

        IEnumerable<TResult> Result<TResult>();
        IEnumerable<TDomain> Result();
    }
    public class QueryDetails
    {
        public int RecordsReturned { get; set; }
        public int TotalRecords { get; set; }
    }
}
