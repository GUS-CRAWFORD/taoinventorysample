﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaoInventory.Models;

namespace TaoInventory.Data
{
    public class SimulatedContext : IDbContext
    {
        public SimulatedContext() {
            if (Products == null) SeedSimulatedContext();
        }

        // The dicitionaries persists throughout one execution of the rest api project
        public static Dictionary<Guid, Product> Products { get; set; }
        public static Dictionary<Guid, Inventory> Inventory { get; set; }

        protected Dictionary<Guid, BaseModel> modifiedRecords = new Dictionary<Guid, BaseModel>();
        public EntityState GetEntryState<TEntity>(TEntity entity) where TEntity : BaseModel
        {
            var myEntity = Set<TEntity>().Find(entity.Id);
            if (myEntity != null) return EntityState.Detached;
            if (modifiedRecords[myEntity.Id] != null) return EntityState.Modified;
            return EntityState.Unchanged;
        }

        public void Reload<TEntity>(TEntity entity) where TEntity : BaseModel { }

        public int SaveChanges()
        {
            // http://msdn.microsoft.com/en-us/library/system.data.entity.dbcontext.savechanges%28v=vs.103%29.aspx
            var result = modifiedRecords.Count;
            modifiedRecords.Clear();
            return result;
        }

        public DbSet<TEntity> Set<TEntity>() where TEntity : BaseModel
        {
            var setType = typeof(TEntity).Name;
            foreach (var property in GetType().GetProperties())
            {
                if (property.Name == setType) return new DbSetSimulator<TEntity>(
                        GetType().GetProperty(property.Name).GetValue(this) as Dictionary<Guid, TEntity>
                    );
            }
            throw new EntityException(String.Format("cannot reflect Dictionary<{0}>", setType));
        }

        public void SetEntryState<TEntity>(TEntity entity, EntityState toState) where TEntity : BaseModel
        {
            if (toState == EntityState.Modified) modifiedRecords.Add(entity.Id, entity);
        }
        public static void SeedSimulatedContext()
        {
            // TODO: Staying within ask, I'm not creating a database, but without that stipulation,
            //          I'd have simply set-up a code-first DbContext pointing at a SQL Express mdf connection string
            // I've not added back-end unit tests, but the DbSetSimulator is based on a common Moq recipe for DbSet:
            // https://github.com/GUSCRAWFORD/SampleReview/blob/release/0.1/SampleReview.DataTests/Util.cs
            Products = new Dictionary<Guid, Product>();
            // TODO: The below would otherwise be accomplished with a 'seeding' DbInitializer
            int seeded = 0, partA = 0, partB = 0;
            var nameParts = new string[] { "Widget", "Box", "Controller", "Analyzer", "Manager", "Glass" };
            var randomizer = new Random();
            while (seeded <= 10)
            {

                var newKey = Guid.NewGuid();
                var newProduct = new Product
                {
                    Id = newKey,
                    Name = nameParts[partA] + " "
                            + nameParts[partB] + "™"
                };
                Products.Add(newKey, newProduct);

                seeded++;
                if (partB < nameParts.Count() - 1) partB++;
                else
                {
                    if (partA < nameParts.Count() - 1) partA++;
                    else partA = 0;
                    partB = 0;
                }

            }
        }
        #region IDisposable Support
        public void Dispose()
        {
            
        }
        #endregion
    }
}
