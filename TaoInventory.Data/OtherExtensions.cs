﻿using System.Linq;
using TaoInventory.Models;

namespace TaoInventory.Data
{
    public static class OtherExtensions
    {
        public static string ToPascalCase(this string name)
        {
            return name.First().ToString().ToUpper() + name.Substring(1);
        }
    }
}
