﻿using System;
using System.Data.Entity;
using TaoInventory.Models;
namespace TaoInventory.Data
{
    public interface IDbContext : IDisposable
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : BaseModel;
        void SetEntryState<TEntity>(TEntity entity, EntityState toState) where TEntity : BaseModel;
        EntityState GetEntryState<TEntity>(TEntity entity) where TEntity : BaseModel;
        void Reload<TEntity>(TEntity entity) where TEntity : BaseModel;
        int SaveChanges();
    }
}
