﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using TaoInventory.Data;
using TaoInventory.Models;

namespace TaoInventory.Api.Controllers
{
    [EnableCors(origins: "http://localhost:65257,http://guscrawford.com", headers: "*", methods: "*")]
    public class ProductsController : ApiController
    {
        // GET: api/Products
        public IEnumerable<Product> Get()
        {
            using (var context = new SimulatedContext())
            {
                return SimulatedContext.Products.Select(hsh => hsh.Value);
            }
        }

        // GET: api/Products/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Products
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Products/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Products/5
        public void Delete(int id)
        {
        }
    }
}
