﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaoInventory.Models
{
    public class Inventory : BaseModel
    {
        public Product Product {get; set;}
        public Guid ProductId { get; set; }
        public int Stock { get; set; }
    }
}
