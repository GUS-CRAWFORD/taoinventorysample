﻿using System;

namespace TaoInventory.Models
{
    public abstract class BaseModel
    {
        public Guid Id { get; set; }
    }
}
