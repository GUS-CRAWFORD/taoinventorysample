# README #

Thanks for inviting me to the second phase of your technical test Mazher, I appreciate the *sample concept*, I will certainly finish this on my own time as it never hurts to have sample source to show off&mdash;I'll be candid though, I've already accepted an offer and I'm essentially unavailable.

I began working on this the night before last, and to be fair I'd want a 2-hour work-day sprint breakdown if a candidate took as long as I have to turn this around.

Having said that, I wanted to show-off some great `Dictionary` use for you :) and I have a neat *simulated* context with only the beginnings of a front-end.

### Tao Inventory Sample ###

* "SimulatedContext" simulating a persistant database by implementing some foundations of EF's DbContext
* A WebApi, and MVC project, with a best-practice integration of Angular build / test / webpack pipeline into ~/Content and managing bundle
* An incomplete front end that merely lists the randomly seeded products (verify the single backend run session to simulated persistence by observing the objects *not* re-generating between refreshes within the same debug session 

### How do I get set up? ###

* Clone this reop
* Open solution
* Run `npm update` in the TaoInventory.com/TaoClientApp folder
* Build and Run