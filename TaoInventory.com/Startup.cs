﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TaoInventory.com.Startup))]
namespace TaoInventory.com
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
