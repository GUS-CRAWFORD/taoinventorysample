webpackJsonp([0],[
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */
/***/ (function(module, exports, __webpack_require__) {


var angular =   __webpack_require__(0);
                __webpack_require__(1);
                __webpack_require__(5);
                
module.exports = angular.module('app', ['ngResource']);

__webpack_require__(4);           


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(6);

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

var angular =   __webpack_require__(0);
                __webpack_require__(2);
                __webpack_require__(3);

module.exports = angular.module('ui.app', ['ngRoute', 'app']);
__webpack_require__(11);
__webpack_require__(13);

angular
    .element(function() {
        angular.bootstrap(document, ['ui.app']);
    });


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0).module('app')
    .service('products', ordersService);

ordersService.$inject = ['$resource', 'apiEndPoint'];

function ordersService ($resource, apiEndPoint) {
    return $resource(apiEndPoint + '/products');
}

/***/ }),
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */
/***/ (function(module, exports) {

module.exports = "<div>\r\n    <div ng-repeat=\"product in $ctrl.products\">\r\n        {{ product.name }}\r\n    </div>\r\n</div>";

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(12);

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0).module('ui.app')
    .component('inventoryCatalog', {
        template: __webpack_require__(10),
        controller:inventoryCatalogController
    });

inventoryCatalogController.$inject=['products'];
function inventoryCatalogController (products) {
    var ctrl = this;
    products.query().$promise.then(function(pr){ctrl.products = pr});
}

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(14);

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0).module('ui.app')
    .config(function($routeProvider) {
        $routeProvider.when('/inventory', {
            template: '<inventory-catalog></inventory-catalog>'
        }).otherwise('/inventory'); // default client view
    });

/***/ })
],[3]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hcHAvYXBwLm1vZHVsZS5qcyIsIndlYnBhY2s6Ly8vLi9hcHAvc2VydmljZXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vdWkvdWkubW9kdWxlLmpzIiwid2VicGFjazovLy8uL2FwcC9zZXJ2aWNlcy9wcm9kdWN0cy5zZXJ2aWNlLmpzIiwid2VicGFjazovLy8uL3VpL2NvbXBvbmVudHMvaW52ZW50b3J5LWNhdGFsb2cvaW52ZW50b3J5LWNhdGFsb2cucGFydGlhbC5odG1sIiwid2VicGFjazovLy8uL3VpL2NvbXBvbmVudHMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vdWkvY29tcG9uZW50cy9pbnZlbnRvcnktY2F0YWxvZy9pbnZlbnRvcnktY2F0YWxvZy5jb21wb25lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vdWkvdmlld3MvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vdWkvdmlld3MvaW52ZW50b3J5LnZpZXcuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsdUI7Ozs7Ozs7QUNQQSx1Qjs7Ozs7O0FDQUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOzs7Ozs7O0FDWEw7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsQzs7Ozs7Ozs7O0FDUEEsMEZBQTBGLGdCQUFnQiwwQjs7Ozs7O0FDQTFHLHdCOzs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0EsZ0RBQWdELG1CQUFtQjtBQUNuRSxDOzs7Ozs7QUNWQSx3Qjs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTLDBCQUEwQjtBQUNuQyxLQUFLLEUiLCJmaWxlIjoidGFvLmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG52YXIgYW5ndWxhciA9ICAgcmVxdWlyZSgnYW5ndWxhcicpO1xyXG4gICAgICAgICAgICAgICAgcmVxdWlyZSgnYW5ndWxhci1yZXNvdXJjZScpO1xyXG4gICAgICAgICAgICAgICAgcmVxdWlyZSgnLi4vdWkvdWkubW9kdWxlJyk7XHJcbiAgICAgICAgICAgICAgICBcclxubW9kdWxlLmV4cG9ydHMgPSBhbmd1bGFyLm1vZHVsZSgnYXBwJywgWyduZ1Jlc291cmNlJ10pO1xyXG5cclxucmVxdWlyZSgnLi9zZXJ2aWNlcycpOyAgICAgICAgICAgXHJcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vYXBwL2FwcC5tb2R1bGUuanNcbi8vIG1vZHVsZSBpZCA9IDNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwicmVxdWlyZSgnLi9wcm9kdWN0cy5zZXJ2aWNlJyk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9hcHAvc2VydmljZXMvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IDRcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwidmFyIGFuZ3VsYXIgPSAgIHJlcXVpcmUoJ2FuZ3VsYXInKTtcclxuICAgICAgICAgICAgICAgIHJlcXVpcmUoJ2FuZ3VsYXItcm91dGUnKTtcclxuICAgICAgICAgICAgICAgIHJlcXVpcmUoJy4uL2FwcC9hcHAubW9kdWxlJyk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IGFuZ3VsYXIubW9kdWxlKCd1aS5hcHAnLCBbJ25nUm91dGUnLCAnYXBwJ10pO1xyXG5yZXF1aXJlKCcuL2NvbXBvbmVudHMnKTtcclxucmVxdWlyZSgnLi92aWV3cycpO1xyXG5cclxuYW5ndWxhclxyXG4gICAgLmVsZW1lbnQoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgYW5ndWxhci5ib290c3RyYXAoZG9jdW1lbnQsIFsndWkuYXBwJ10pO1xyXG4gICAgfSk7XHJcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vdWkvdWkubW9kdWxlLmpzXG4vLyBtb2R1bGUgaWQgPSA1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInJlcXVpcmUoJ2FuZ3VsYXInKS5tb2R1bGUoJ2FwcCcpXHJcbiAgICAuc2VydmljZSgncHJvZHVjdHMnLCBvcmRlcnNTZXJ2aWNlKTtcclxuXHJcbm9yZGVyc1NlcnZpY2UuJGluamVjdCA9IFsnJHJlc291cmNlJywgJ2FwaUVuZFBvaW50J107XHJcblxyXG5mdW5jdGlvbiBvcmRlcnNTZXJ2aWNlICgkcmVzb3VyY2UsIGFwaUVuZFBvaW50KSB7XHJcbiAgICByZXR1cm4gJHJlc291cmNlKGFwaUVuZFBvaW50ICsgJy9wcm9kdWN0cycpO1xyXG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9hcHAvc2VydmljZXMvcHJvZHVjdHMuc2VydmljZS5qc1xuLy8gbW9kdWxlIGlkID0gNlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJtb2R1bGUuZXhwb3J0cyA9IFwiPGRpdj5cXHJcXG4gICAgPGRpdiBuZy1yZXBlYXQ9XFxcInByb2R1Y3QgaW4gJGN0cmwucHJvZHVjdHNcXFwiPlxcclxcbiAgICAgICAge3sgcHJvZHVjdC5uYW1lIH19XFxyXFxuICAgIDwvZGl2PlxcclxcbjwvZGl2PlwiO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vdWkvY29tcG9uZW50cy9pbnZlbnRvcnktY2F0YWxvZy9pbnZlbnRvcnktY2F0YWxvZy5wYXJ0aWFsLmh0bWxcbi8vIG1vZHVsZSBpZCA9IDEwXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInJlcXVpcmUoJy4vaW52ZW50b3J5LWNhdGFsb2cvaW52ZW50b3J5LWNhdGFsb2cuY29tcG9uZW50Jyk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi91aS9jb21wb25lbnRzL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAxMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJyZXF1aXJlKCdhbmd1bGFyJykubW9kdWxlKCd1aS5hcHAnKVxyXG4gICAgLmNvbXBvbmVudCgnaW52ZW50b3J5Q2F0YWxvZycsIHtcclxuICAgICAgICB0ZW1wbGF0ZTogcmVxdWlyZSgnLi9pbnZlbnRvcnktY2F0YWxvZy5wYXJ0aWFsLmh0bWwnKSxcclxuICAgICAgICBjb250cm9sbGVyOmludmVudG9yeUNhdGFsb2dDb250cm9sbGVyXHJcbiAgICB9KTtcclxuXHJcbmludmVudG9yeUNhdGFsb2dDb250cm9sbGVyLiRpbmplY3Q9Wydwcm9kdWN0cyddO1xyXG5mdW5jdGlvbiBpbnZlbnRvcnlDYXRhbG9nQ29udHJvbGxlciAocHJvZHVjdHMpIHtcclxuICAgIHZhciBjdHJsID0gdGhpcztcclxuICAgIHByb2R1Y3RzLnF1ZXJ5KCkuJHByb21pc2UudGhlbihmdW5jdGlvbihwcil7Y3RybC5wcm9kdWN0cyA9IHByfSk7XHJcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3VpL2NvbXBvbmVudHMvaW52ZW50b3J5LWNhdGFsb2cvaW52ZW50b3J5LWNhdGFsb2cuY29tcG9uZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAxMlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJyZXF1aXJlKCcuL2ludmVudG9yeS52aWV3Jyk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi91aS92aWV3cy9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMTNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwicmVxdWlyZSgnYW5ndWxhcicpLm1vZHVsZSgndWkuYXBwJylcclxuICAgIC5jb25maWcoZnVuY3Rpb24oJHJvdXRlUHJvdmlkZXIpIHtcclxuICAgICAgICAkcm91dGVQcm92aWRlci53aGVuKCcvaW52ZW50b3J5Jywge1xyXG4gICAgICAgICAgICB0ZW1wbGF0ZTogJzxpbnZlbnRvcnktY2F0YWxvZz48L2ludmVudG9yeS1jYXRhbG9nPidcclxuICAgICAgICB9KS5vdGhlcndpc2UoJy9pbnZlbnRvcnknKTsgLy8gZGVmYXVsdCBjbGllbnQgdmlld1xyXG4gICAgfSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi91aS92aWV3cy9pbnZlbnRvcnkudmlldy5qc1xuLy8gbW9kdWxlIGlkID0gMTRcbi8vIG1vZHVsZSBjaHVua3MgPSAwIl0sInNvdXJjZVJvb3QiOiIifQ==