var angular =   require('angular');
                require('angular-route');
                require('../app/app.module');

module.exports = angular.module('ui.app', ['ngRoute', 'app']);
require('./components');
require('./views');

angular
    .element(function() {
        angular.bootstrap(document, ['ui.app']);
    });
