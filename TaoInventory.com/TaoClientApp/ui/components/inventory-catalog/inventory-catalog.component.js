require('angular').module('ui.app')
    .component('inventoryCatalog', {
        template: require('./inventory-catalog.partial.html'),
        controller:inventoryCatalogController
    });

inventoryCatalogController.$inject=['products'];
function inventoryCatalogController (products) {
    var ctrl = this;
    products.query().$promise.then(function(pr){ctrl.products = pr});
}