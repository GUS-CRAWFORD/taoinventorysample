require('angular').module('ui.app')
    .config(function($routeProvider) {
        $routeProvider.when('/inventory', {
            template: '<inventory-catalog></inventory-catalog>'
        }).otherwise('/inventory'); // default client view
    });