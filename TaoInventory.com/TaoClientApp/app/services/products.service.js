require('angular').module('app')
    .service('products', ordersService);

ordersService.$inject = ['$resource', 'apiEndPoint'];

function ordersService ($resource, apiEndPoint) {
    return $resource(apiEndPoint + '/products');
}