require('angular').module('app')
    .service('orders', ordersService);

ordersService.$inject = ['$resource', 'apiEndPoint'];

function ordersService ($resource, apiEndPoint) {
    return $resource(apiEndPoint + '/foo');
}